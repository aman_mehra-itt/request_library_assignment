import send_request
import verification
import read_input


"""import http.client
http.client.HTTPConnection.debuglevel = 5"""


url = "https://jsonplaceholder.typicode.com"


if __name__=="__main__":
    Input=read_input.Read_input()
    request=send_request.Request()
    verify=verification.Verification()

    flag=True
    while(flag):
        option=Input.main_menu()
        if option==1:
            para={}
            flagg=True
            while(flagg):
                choosed_parameter=Input.parameter_for_get_request()
                if choosed_parameter==1:
                    value=int(input("enter the value : "))
                    para["postId"]=value
                elif choosed_parameter==2:
                    value=int(input("enter the value : "))
                    para["id"]=value
                elif choosed_parameter==3:
                    value=input("enter the value : ")
                    para["name"]=value
                elif choosed_parameter==4:
                    value=input("enter the value : ")
                    para["email"]=value
                elif choosed_parameter==5:
                    value=input("enter the value : ")
                    para["body"]=value
                elif choosed_parameter==6:
                    flagg=False
            url_get=url+"/comments"
            get_response=request.get_request(url_get,para)
            print("get request successfull")
            verify.get_request_parameters(get_response,para)
            verify.status_code(get_response,200)
            url_get=url_get+"?"
            count=0
            for i in para:
                count+=1
                if count==len(para):
                    url_get=url_get +str(i)+"="+str(para[i])
                elif count<len(para):
                    url_get=url_get +str(i)+"="+str(para[i])+"&"
            verify.url(get_response,url_get)
        elif option==2:
            url_post=url+"/posts"
            post_json_data={"title": "aman","body": "good","userId": 1}
            post_xml_data="""<?xml version='1.0' encoding='utf-8'?><title>Anythin</title><body>anything in boody</body><userId>34</userId>"""
            xml_header={'Content-Type': 'application/xml'}
            post_response_json=request.post_request_json(url_post,post_json_data)
            post_response_xml=request.post_request_xml(url_post,post_xml_data,xml_header)
            print("post request successfull")
            verify.post_request_payload(post_response_xml,post_xml_data)
            verify.status_code(post_response_json,201)
            verify.url(post_response_json,url_post)
            verify.status_code(post_response_xml,201)
            verify.url(post_response_xml,url_post)
        elif option==3:
            url_put=url+"/posts/1"
            put_data={"id": 1,"title": "aman","body": "good","userId": 1}
            put_response=request.put_request(url_put,put_data)
            print("put request successfull")
            verify.status_code(put_response,200)
            verify.url(put_response,url_put)
        elif option==4:
            flag=False