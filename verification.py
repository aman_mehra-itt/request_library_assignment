
url = "https://jsonplaceholder.typicode.com"


class Verification:
    def status_code(self,response,expected):
        assert response.status_code==expected,"status code not matched"
        print("status code matching")
    
    def url(self,response,expected):
        print(response.url)
        print(expected)
        assert response.url==expected,"url not matched"
        print("url is matching")

    def post_request_payload(self,response,expected):
        assert response.request.body==expected,"Invalid payload"
        print("valid payload")


    def get_request_parameters(self,response,para):
        data=response.json()
        flagg=0
        for i in para:
            temp=0
            while(temp<len(data)):
                if data[temp][i]==para[i]:
                    flagg=1
                else:
                    flagg=0
                    break
                temp+=1
            if flagg==0:
                print("invalid data recieved")
                return
        if flagg==1:
            print("valid data recieved")
        elif flagg==0:
            print("invalid data recieved")