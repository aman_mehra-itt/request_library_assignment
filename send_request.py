import requests

class Request:
    def get_request(self,url,para):
        response=requests.get(url,params=para)
        return response

    def post_request_json(self,url,data=None):
        response=requests.post(url, json=data)
        return response
    
    def post_request_xml(self,url,data=None,headers=None):
        response=requests.post(url,data=data,headers=headers)
        return response
    
    def put_request(self,url,data=None):
        response=requests.put(url,data=data)
        return response
